clear
echo "[ DEPLOY START ]"
yarn build --prod --configuration=production &&
aws s3 sync ./dist s3://ubisens.cf-air.com --acl public-read &&
aws s3 sync ./dist s3://www.ubisens.com --acl public-read &&
aws s3 sync ./dist s3://ubisens.com --acl public-read
echo "[ DEPLOY DONE ]"