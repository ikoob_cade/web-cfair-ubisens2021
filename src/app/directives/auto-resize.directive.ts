// Usage example: <textarea autoresize></textarea>

import { Directive, HostListener, ElementRef, OnInit } from '@angular/core';

@Directive({
    selector: '[autosize]'
})
export class AutosizeDirective implements OnInit {

    constructor(private elementRef: ElementRef) { }

    @HostListener(':input')
    onInput(): void {
        this.resize();
    }

    ngOnInit(): void {
        if (this.elementRef.nativeElement.scrollHeight) {
            setTimeout(() => this.resize());
        }
    }

    resize(): void {
        this.elementRef.nativeElement.style.height = '0';
        this.elementRef.nativeElement.style.height = this.elementRef.nativeElement.scrollHeight + 'px';
    }
}
