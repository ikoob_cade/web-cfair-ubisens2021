import { DeviceDetectorService } from 'ngx-device-detector';
import { NgImageSliderModule } from 'ng-image-slider';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CookieService } from 'ngx-cookie-service';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule, COMPOSITION_BUFFER_MODE } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe } from '@angular/common';
import { SocketService } from './services/socket/socket.service';
import { NgxPaginationModule } from 'ngx-pagination';

// Service
import { ApiService } from './services/api/api.service';
import { AuthService } from './services/auth/auth.service';
import { AuthInterceptorService } from './services/auth-interceptor/auth-interceptor.service';
import { EventService } from './services/api/event.service';
import { BannerService } from './services/api/banner.service';
import { DataService } from './services/data.service';

// Pages
import { AppComponent } from './app.component';
import { MainComponent } from './pages/main/main.component';
import { BannerComponent } from './components/banner/banner.component';

import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

// Components
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SafeHtmlPipe } from './pipes/safe-html/safe-html.pipe';
import { LinkifyPipe } from './pipes/linkify/linkify.pipe';
import { NotSupportIeComponent } from './pages/not-support-ie/not-support-ie.component';

import { AutosizeDirective } from './directives/auto-resize.directive';
import { DayjsService } from './services/dayjs.service';
import { FunctionService } from './services/function/function.service';
import { SliderControllerService } from './services/function/sliderController.service';

const pages = [
  MainComponent,
  PageNotFoundComponent,
];

const components = [
  HeaderComponent,
  FooterComponent,
  BannerComponent,
  NotSupportIeComponent,
];

const pipes = [
  SafeHtmlPipe,
  LinkifyPipe,
];

@NgModule({
  declarations: [
    AppComponent,
    AutosizeDirective,
    ...pages,
    ...components,
    ...pipes,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgImageSliderModule,
    PdfViewerModule,
    NgxPaginationModule,
  ],
  providers: [
    ApiService,
    AuthService,
    BannerService,
    DatePipe,
    EventService,
    DeviceDetectorService,
    SocketService,
    CookieService,
    DataService,
    DayjsService,
    FunctionService,
    SliderControllerService,
    PdfViewerModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    },
    { provide: COMPOSITION_BUFFER_MODE, useValue: false },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
