import { Injectable } from "@angular/core";

@Injectable()
export class SliderControllerService {
    constructor() {
    }

    // 광고 구좌 왼쪽 버튼 클릭
    slidePrev(comp, target): any {
        comp[target].prev();
    }

    // 광고 구좌 오른쪽 버튼 클릭
    slideNext(comp, target): void {
        comp[target].next();
    }

    imageClick(banners, index): void {
        console.log(banners);
        if (banners[index] && banners[index].link) {
            window.open(banners[index].link);
        }
    }


}
