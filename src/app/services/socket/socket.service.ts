import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as io from 'socket.io-client';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  // private url = 'https://cfair-socket-ssl.cf-air.com'; // 운영
  private url = environment.socket_url; // 운영
  // private url = 'http://192.168.1.11:38038'; // 로컬서버가 띄운 소켓서버

  private socket;
  private user;
  private currentRoomId;
  private memberIdLive;
  public receiveMessage = new BehaviorSubject(false);

  constructor(
    public router: Router,
    private dataService: DataService,
  ) {
  }

  init(): void {
    this.socket = io(this.url, {
      transports: ['websocket']
    });

    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    // 소켓 연결
    this.socket.on('connect', () => {
      console.log('socket server on.');
    });

    // 소켓 연결 해제
    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });

    this.socket.on('message', (data) => {

      if (data.eventId === '618217e762e1790012cdcddf') {
        this.dataService.changeReceiveMessage(data);
      }
    });

    // multiViewing
    this.socket.on('multiViewing', (data) => {
      this.router.navigate(['/']);
    });
  }
}
