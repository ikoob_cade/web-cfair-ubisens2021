import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { EventService } from '../../services/api/event.service';
import { BannerService } from '../../services/api/banner.service';
import { SliderControllerService } from '../../services/function/sliderController.service';
import { UAParser } from 'ua-parser-js';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { NgImageSliderComponent } from 'ng-image-slider';
import { DayjsService } from '../../services/dayjs.service';
declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  private serverUrl = '/events/:eventId/open-comments';

  @ViewChild('confirmPasswordAlertBtn') confirmPasswordAlertBtn: ElementRef; // 비밀번호 확인 모달 On
  @ViewChild('cancelPasswordConfirmAlertBtn') cancelPasswordConfirmAlertBtn: ElementRef; // 비밀번호 확인 모달 Off

  @ViewChild('updateCommentAlertBtn') updateCommentAlertBtn: ElementRef;
  @ViewChild('cancelUpdateCommentAlertBtn') cancelUpdateCommentAlertBtn: ElementRef;

  private isOnlyNumberRegex = /[^0-9]/g;

  public mobile: boolean;
  event;
  public banners = [];

  curPage = 1;
  itemsPerPage = 20;
  totalPagesCount;


  public newInput = {
    writer: '',
    phone: '',
    password: '',
    description: ''
  };

  public updateInput = {
    writer: '',
    phone: '',
    password: '',
    description: '',
  };

  selectedOpenComment; // 삭제, 수정 선택 아이템;

  public comments = [];
  public count = 0;

  confirmPassword;
  public isModify = false;

  constructor(
    private http: HttpClient,
    // private cookieService: CookieService,
    // private functionService: FunctionService,
    private eventService: EventService,
    private bannerService: BannerService,
    private sliderControllerService: SliderControllerService,
    private dayJsService: DayjsService
  ) { }

  clearUpdateCommentAlert(): void {
    this.updateInput = {
      writer: '',
      phone: '',
      password: '',
      description: '',
    };
  }


  updateComment(): void {
    const body = {
      writer: this.updateInput.writer,
      phone: this.updateInput.phone,
      password: this.updateInput.password,
      description: this.updateInput.description,
    };

    this.update(this.selectedOpenComment.id, body).subscribe(res => {
      alert('수정되었습니다.');
      this.clearUpdateCommentAlert();
      this.cancelUpdateCommentAlertBtn.nativeElement.click();
      this.getOpenComments();
    });
  }

  clearInput(): void {
    this.newInput = {
      writer: '',
      phone: '',
      password: '',
      description: ''
    };
  }

  ngOnInit(): void {
    this.checkEventVersion();
    this.getBanners();

  }

  ngAfterViewInit(): void {
    this.setSystemMaintenance();
  }

  setSystemMaintenance(): void {
    const first = this.dayJsService.makeTime('2021-11-09 15:00');
    const end = this.dayJsService.makeTime('2021-11-09 17:00');
    const now = this.dayJsService.makeTime();

    if (now.isSameOrAfter(first) && now.isSameOrBefore(end)) {
      // ! 메인 팝업
      setTimeout(() => {
        this.entryAlert.nativeElement.click();
      }, 0);
    }

  }


  onlyNumber(event): void {
    const inputVal = event.target.value;
    event.target.value = inputVal.replace(/[^0-9]/gi, '');
  }

  // 버전확인
  checkEventVersion(): void {
    this.eventService.findOne().subscribe(res => {
      this.event = res;
      if (this.event.clientVersion) {
        const clientVersion = localStorage.getItem(this.event.eventCode + 'ver');
        if (!clientVersion) {
          localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
          location.reload();
        } else if (clientVersion) {
          if (clientVersion !== this.event.clientVersion) {
            localStorage.setItem(this.event.eventCode + 'ver', this.event.clientVersion);
            location.reload();
          }
        }
      }
    });
  }


  /** 답글 Input 활성화 */
  openReply(selectedCommentId): void {
    const cInputWrapper = $(`#cInput_wrapper_${selectedCommentId}`)[0];
    if (cInputWrapper.style.display === 'none' || !cInputWrapper.style.display) {
      cInputWrapper.style.display = 'block';
      $(`#cInput_${selectedCommentId}_description`)[0].focus();
    } else {
      cInputWrapper.style.display = 'none';
    }
  }

  getOpenComments(): void {
    const params = {
      page: this.curPage,
      pageSize: this.itemsPerPage
    };
    this.findAll(params).subscribe(res => {
      this.comments = res.comments;
      this.count = res.totalCount;
      this.totalPagesCount = res.totalPagesCount;
    });
  }

  cancel(): void {
    this.getOpenComments();
  }

  modify(comment): void {
    this.selectedOpenComment = comment;
  }

  // 댓글 생성
  save(parentId?: string): void {
    if (this.validateComment(parentId ? `#cInput_${parentId}` : null)) {
      const parser = new UAParser();
      const fullUserAgent = parser.getResult();

      const body: any = {
        password: parentId ? $(`#cInput_${parentId}_password`)[0].value : this.newInput.password,
        phone: parentId ? $(`#cInput_${parentId}_phone`)[0].value : this.newInput.phone,
        writer: parentId ? $(`#cInput_${parentId}_writer`)[0].value : this.newInput.writer,
        description: parentId ? $(`#cInput_${parentId}_description`)[0].value : this.newInput.description,
        parentId: parentId ? parentId : '',
        level: parentId ? 1 : 0,
        userAgent: JSON.stringify(fullUserAgent),
        browser: JSON.stringify(fullUserAgent.browser),
        device: JSON.stringify(fullUserAgent.device),
        engine: JSON.stringify(fullUserAgent.engine),
        os: JSON.stringify(fullUserAgent.os),
        ua: JSON.stringify(fullUserAgent.ua),
      };

      this.create(body).subscribe(res => {
        alert('등록되었습니다.');
        if (!parentId) { this.curPage = 1; }
        this.clearInput();
        this.getOpenComments();
      });
    }
  }

  changePage(page): void {
    this.curPage = page;
    this.getOpenComments();
  }

  // ! 비밀번호 확인 열기
  passwordConfirm(openCommentId, isModify: boolean): void {
    this.selectedOpenComment = openCommentId;
    this.isModify = isModify;
    this.confirmPassword = '';
    this.confirmPasswordAlertBtn.nativeElement.click();
  }

  // ! 수정 비밀번호 확인
  checkPassword(): void {
    if (!this.confirmPassword) {
      return alert('비밀번호를 입력해주세요.');
    }

    this.check(this.selectedOpenComment.id, this.confirmPassword).subscribe(res => {
      if (res.result) {
        const updatePassword = this.confirmPassword;
        this.updateInput.writer = this.selectedOpenComment.writer;
        this.updateInput.phone = res.phone;
        this.updateInput.password = updatePassword;
        this.cancelPasswordConfirmAlertBtn.nativeElement.click();
        this.updateCommentAlertBtn.nativeElement.click();
        this.updateInput.description = this.selectedOpenComment.description;
        this.clearConfirmPasswordAlert();

      } else {
        alert('비밀번호 불일치');
      }
    });
  }

  // ! 댓글 삭제
  remove(): void {
    if (!this.confirmPassword) {
      return alert('비밀번호를 입력해주세요.');
    }

    this.delete(this.selectedOpenComment.id, this.confirmPassword).subscribe(res => {
      alert('삭제되었습니다.');
      this.clearConfirmPasswordAlert();
      this.cancelPasswordConfirmAlertBtn.nativeElement.click();
      this.getOpenComments();
      this.selectedOpenComment = null;
    }, error => {
      // tslint:disable-next-line: no-unused-expression
      error.error ? alert(error.error.message) : '';
    });
  }

  clearConfirmPasswordAlert(): void {
    this.confirmPassword = '';
  }


  // ! 댓글 생성 유효성 검사
  validateComment(parentElement?): boolean {
    if (!parentElement) {
      if (!this.newInput.writer) {
        alert('이름을 입력해주세요.');
        return false;
      }
      this.newInput.phone = this.newInput.phone.replace(/[^0-9]/gi, '');
      if (!this.newInput.phone) {
        alert('휴대전화를 입력해주세요.');
        return false;
      }
      if (this.newInput.phone.length < 10) {
        alert('휴대전화는 최소 10자리여야합니다.');
        return false;
      }
      if (!this.newInput.password) {
        alert('비밀번호를 입력해주세요.');
        return false;
      }
      if (!this.newInput.description) {
        alert('댓글을 입력해주세요.');
        return false;
      }
      if (this.newInput.password.length < 4) {
        alert('비밀번호 4자리를 입력해주세요.');
        return false;
      }

      if (this.isOnlyNumberRegex.test(this.newInput.password)) {
        alert('비밀번호는 숫자만 가능합니다.');
        return false;
      }
      return true;
    } else {
      const childInput = {
        writer: $(`${parentElement}_writer`)[0].value,
        password: $(`${parentElement}_password`)[0].value,
        description: $(`${parentElement}_description`)[0].value,
        phone: $(`${parentElement}_phone`)[0].value
      };
      if (!childInput.writer) {
        alert('이름을 입력해주세요.');
        return false;
      }
      childInput.phone = childInput.phone.replace(/[^0-9]/gi, '');
      if (!childInput.phone) {
        alert('휴대전화를 입력해주세요.');
        return false;
      }
      if (childInput.phone.length < 10) {
        alert('휴대전화는 최소 10자리여야합니다.');
        return false;
      }
      if (!childInput.password) {
        alert('비밀번호를 입력해주세요.');
        return false;
      }
      if (!childInput.description) {
        alert('답글을 입력해주세요.');
        return false;
      }
      if (childInput.password.length < 4) {
        alert('비밀번호 4자리를 입력해주세요.');
        return false;
      }
      if (this.isOnlyNumberRegex.test(childInput.password)) {
        alert('비밀번호는 숫자만 가능합니다.');
        return false;
      }
      return true;
    }
  }

  // ! Banner Slider
  /** 배너 목록 조회 */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      res.forEach(item => {
        const data = {
          link: item.link,
          thumbImage: item.photoUrl,
          alt: item.title,
        };
        this.banners.push(data);
      });
    });

    this.getOpenComments();
  }
  /** 광고 구좌 왼쪽 버튼 클릭 */
  slidePrev(target): void {
    this.sliderControllerService.slidePrev(this, target);
  }
  /** 광고 구좌 오른쪽 버튼 클릭 */
  slideNext(target): void {
    this.sliderControllerService.slideNext(this, target);
  }
  /** 광고 배너 클릭 */
  imageClick(index): void {
    this.sliderControllerService.imageClick(this.banners, index);
  }


  // ! APIS
  create(body: any): Observable<any> {
    return this.http.post(this.serverUrl, body)
      .pipe(catchError(this.handleError));
  }

  findAll(params: any): Observable<any> {
    return this.http.get(this.serverUrl, { params })
      .pipe(catchError(this.handleError));
  }

  update(openCommentId: string, body): Observable<any> {
    return this.http.put(this.serverUrl + '/' + openCommentId, body);
  }

  delete(openCommentId: string, password: string): Observable<any> {
    return this.http.delete(this.serverUrl + '/' + openCommentId, { params: { password } });
  }

  check(openCommentId: string, password: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + openCommentId + '/check', { params: { password } })
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}