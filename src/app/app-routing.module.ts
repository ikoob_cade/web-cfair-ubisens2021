import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Service
import { AttendGuard } from './services/attend-guard/attend-guard.service';

//Page
import { MainComponent } from './pages/main/main.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { NotSupportIeComponent } from './pages/not-support-ie/not-support-ie.component';

const routes: Routes = [
  { path: 'main', component: MainComponent, canActivate: [AttendGuard] },
  { path: 'not-ie', component: NotSupportIeComponent },
  { path: '', redirectTo: '/main', pathMatch: 'full', canActivate: [AttendGuard] },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [
    // RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
    RouterModule.forRoot(routes, { enableTracing: false, onSameUrlNavigation: 'reload' }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
