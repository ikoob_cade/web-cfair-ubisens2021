FROM node:12.21-alpine AS build
RUN mkdir -p /app
WORKDIR /app

COPY package*.json /app
RUN npm install
RUN npm install -g @angular/cli@10.0.4

COPY . /app
RUN ng build --configuration=dev

FROM bitnami/nginx:latest
COPY --from=build app/dist /app